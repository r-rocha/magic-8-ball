// definindo o namespace do svg
var svgNS = "http://www.w3.org/2000/svg";

/* 
// desenhando a tela para ser pintada
var canvasSVG = document.createElementNS(svgNS, "svg");
canvasSVG.setAttributeNS(null,"id","canvas-svg");
// canvasSVG.setAttributeNS(null, "viewBox", "0 0 100 100");

// apendando o elemento no corpo do SVG
document.body.appendChild(canvasSVG);

// desenhando o círculo
var myCircle = document.createElementNS(svgNS,"circle"); 
myCircle.setAttributeNS(null,"id","mycircle");
myCircle.setAttributeNS(null,"cx",200);
myCircle.setAttributeNS(null,"cy",200);
myCircle.setAttributeNS(null,"r",100);
myCircle.setAttributeNS(null,"fill","black");

// desenhado o círculo menor
var smallCircle = document.createElementNS(svgNS,"circle"); 
smallCircle.setAttributeNS(null,"id","mini-ball");
smallCircle.setAttributeNS(null,"cx",200);
smallCircle.setAttributeNS(null,"cy",200);
smallCircle.setAttributeNS(null,"r",40);
smallCircle.setAttributeNS(null,"stroke", "white")
smallCircle.setAttributeNS(null,"fill","white");

// inserindo o número oito no círculo
var numberEight = document.createElementNS(svgNS,"text"); 
numberEight.setAttributeNS(null,"id","number");
numberEight.setAttributeNS(null,"x",30);
numberEight.setAttributeNS(null,"y",66);
numberEight.setAttributeNS(null,"font-size",60);
numberEight.setAttributeNS(null,"text-anchor", "middle");
numberEight.setAttributeNS(null,"fill","blue");
numberEight.insertAdjacentText("afterend", "8");

// apendando o elemento no canvas criado
document.getElementById("canvas-svg").appendChild(myCircle);

// apendando o elemento no mycircle criado
document.getElementById("canvas-svg").appendChild(smallCircle);

// apendando o número '8' no smallCircle criado
document.getElementById("canvas-svg").appendChild(numberEight); */

let QuoteArray = 
    [
    "Try again.",
    "It is Certain.",
    "It is decidedly so.",
    "Without a doubt.",
    "Yes definitely.",
    "You may rely on it.",
    "As I see it, yes.",
    "Most likely.",
    "Outlook good.",
    "Yes.",
    "Signs point to yes.",
    "Reply hazy, try again.",
    "Ask again later.",
    "Better not tell you now.",
    "Cannot predict now.",
    "Concentrate and ask again",
    "Don't count on it.",
    "My reply is no.",
    "My sources say no.",
    "Outlook not so good.",
    "Very doubtful.",
    ]

let quotes = QuoteArray.length;

console.log(QuoteArray);
console.log(quotes);


let button = document.getElementById("play");
console.log(button);

function triangulo() {

    let triangulo = document.getElementById("triangle");
    let texto = document.getElementById("number");

    if (triangulo != null) {
        triangulo.remove();
    }

    if (texto != null) {
        texto.remove();
    }
        
    let triangle = document.createElementNS(svgNS, "polygon");
    triangle.setAttributeNS(null, "id", "triangle");
    triangle.setAttributeNS(null, "fill", "blue");
    triangle.setAttributeNS(null, "stroke", "#fff");
    triangle.setAttributeNS(null, "stroke-width", 2);
    triangle.setAttributeNS(null, "points", "320,130, 80,130, 200,335");
    // console.log(triangle);
    document.getElementById("canvas-svg").appendChild(triangle);
    var newTriangle = document.getElementById("triangle");
    return newTriangle;
}

button.addEventListener("click", function () {
    index = Math.floor(Math.random() * 22) + 0;
    console.log(index);

    // Cria o triângulo de resposta
    triangulo();
    
    var answer = document.getElementById("triangle");
    console.log(answer);
    
    switch (index) {
        case 1:
            console.log(QuoteArray[1]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[1] + '</text>');
            break;
        case 2:
            console.log(QuoteArray[2]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[2] + '</text>');
            break;
        case 3:
            console.log(QuoteArray[3]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[3] + '</text>');
            break;
        case 4:
            console.log(QuoteArray[4]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[4] + '</text>');
            break;
        case 5:
            console.log(QuoteArray[5]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[5] + '</text>');
            break;
        case 6:
            console.log(QuoteArray[6]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[6] + '</text>');
            break;
        case 7:
            console.log(QuoteArray[7]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[7] + '</text>');
            break;
        case 8:
            console.log(QuoteArray[8]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[8] + '</text>');
            break;
        case 9:
            console.log(QuoteArray[9]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[9] + '</text>');
            break;
        case 10:
            console.log(QuoteArray[10]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[10] + '</text>');
            break;
        case 11:
            console.log(QuoteArray[11]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[11] + '</text>');
            break;
        case 12:
            console.log(QuoteArray[12]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[12] + '</text>');
            break;
        case 13:
            console.log(QuoteArray[13]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[13] + '</text>');
            break;
        case 14:
            console.log(QuoteArray[14]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[14] + '</text>');
            break;
        case 15:
            console.log(QuoteArray[15]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[15] + '</text>');
            break;
        case 16:
            console.log(QuoteArray[16]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[16] + '</text>');
            break;
        case 17:
            console.log(QuoteArray[17]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[17] + '</text>');
            break;
        case 18:
            console.log(QuoteArray[18]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[18] + '</text>');
            break;
        case 19:
            console.log(QuoteArray[19]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[19] + '</text>');
            break;
        case 20:
            console.log(QuoteArray[20]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[20] + '</text>');
            break;
        case 21:
            console.log(QuoteArray[21]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[21] + '</text>');
            break;
        case 22:
            console.log(QuoteArray[22]);
            answer.insertAdjacentHTML("afterend", '<text id="number" x="125" y="190" font-size="13" fill="#fff">' + QuoteArray[22] + '</text>');
            break;
        default:
            console.log(QuoteArray[0]);
    }
});

var buttonRefresh = document.getElementById("reset");

buttonRefresh.addEventListener("click", function () {
    document.getElementById("canvas-svg").innerHTML = "";
    document.getElementById("canvas-svg").innerHTML = '<circle id="mycircle" cx="200" cy="200" r="150" fill="black"></circle> <circle id="mini-ball" cx="200" cy="200" r="65" stroke="white" fill="white"></circle> <text id="number" x="200" y="230" font-size="70" text-anchor="middle" fill="black">8</text>';
});